#!/bin/bash

# setup repos
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf install -y fedora-workstation-repositories rpmfusion-free-release-tainted
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# edit dnf.conf
echo 'fastestmirror=true' >> /etc/dnf/dnf.conf
echo 'max_parallel_downloads=20' >> /etc/dnf/dnf.conf

# updates
dnf upgrade -y

# Apps
dnf remove -y \
	cheese \

dnf install -y \
	keepassxc \
	nextcloud-client \
	nextcloud-client-nautilus \
	timeshift \
	torbrowser-launcher \
	chromium-browser-privacy \
	gnome-tweaks \
	zoxide \
	exa \
	gnome-extensions-app \
	gnome-shell-extension-gsconnect \
	gnome-shell-extension-appindicator \
	gnome-shell-extension-user-theme \
	zsh \
	starship \
	zsh-autosuggestions \
	zsh-syntax-highlighting \
	vim \
	fira-code-fonts \
	libdvdcss \
	util-linux-user \

flatpak install -y flathub io.freetubeapp.FreeTube org.signal.Signal

# setup subvolumes for timeshift
mapper_ID=$(ls /dev/mapper | sort | grep luks)
mkdir /btrfs_pool
mount -o subvolid=5 "/dev/mapper/$mapper_ID" /btrfs_pool
mv /btrfs_pool/root /btrfs_pool/@
mv /btrfs_pool/home /btrfs_pool/@home
sed -i 's/subvol=root/subvol=@,ssd,noatime,space_cache,commit=120,compress=zstd,discard=async/' /etc/fstab
sed -i 's/subvol=home/subvol=@home,ssd,noatime,space_cache,commit=120,compress=zstd,discard=async/' /etc/fstab
echo "UUID=$(blkid -s UUID -o value "/dev/mapper/$mapper_ID")   /btrfs_pool   btrfs   subvolid=5,ssd,noatime,space_cache,commit=120,compress=zstd,discard=async,x-systemd.device-timeout=0   0 0" >> /etc/fstab
grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
grub2-mkconfig -o /boot/grub2/grub.cfg

# more specialized optional software
echo "Which E-Mail client do you want?"
select opt in "Evolution" "Thunderbird"; do
	case $opt in
		"Evolution" ) dnf install -y evolution; break;;
		"Thunderbird" ) dnf install -y thunderbird; break;;
	esac
done

echo "Do you want to install gaming software?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) dnf install -y wine lutris && dnf install -y steam --enablerepo=rpmfusion-nonfree-steam; break;;
		No ) break;;
	esac
done

echo "Do you want to install streaming/recording software?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) dnf install -y obs-studio handbrake-gui; break;;
		No ) break;;
	esac
done

echo "Do you want to install development software?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) rpm --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg && printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=gitlab.com_paulcarroty_vscodium_repo\nbaseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg" |sudo tee -a /etc/yum.repos.d/vscodium.repo; dnf copr enable -y phracek/PyCharm && dnf install -y codium pycharm-community; curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh; break;;
		No ) break;;
	esac
done
