# themes 
# curl -LO https://github.com/EliverLara/Nordic/releases/download/v1.9.0/Nordic-bluish-accent.tar.xz 
curl -Ls https://api.github.com/repos/EliverLara/Nordic/releases/latest | grep -wo "https.*Nordic-bluish-accent.tar.xz" | wget -qi -

tar -xf Nordic-bluish-accent.tar.xz && mkdir -p ~/.themes && mv Nordic-bluish-accent ~/.themes && rm -r Nordic-bluish-accent.tar.xz

curl -LO https://github.com/zayronxio/Zafiro-icons/archive/refs/tags/1.1.tar.gz && tar -xf 1.1.tar.gz && mkdir -p ~/.icons && mv Zafiro-icons-1.1 ~/.icons/Zafiro-icons

gsettings set org.gnome.shell.extensions.user-theme name 'Nordic-bluish-accent'
gsettings set org.gnome.desktop.interface gtk-theme 'Nordic-bluish-accent'
gsettings set org.gnome.desktop.interface icon-theme 'Zafiro-icons'

# gnome settings
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
gnome-extensions enable gsconnect@andyholmes.github.io

# window decorations and keyboard settings
gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:minimize,maximize,close'
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:escape']"
gsettings set org.gnome.desktop.wm.keybindings close "['<Super>q']"

# navigate workspaces
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-1 "['<Shift><Super><exclam']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-2 "['<Shift><Super><quotedbl']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-3 "['<Shift><Super><section']"
gsettings set org.gnome.desktop.wm.keybindings move-to-workspace-4 "['<Shift><Super><dollar']"

# open terminal with Super+Return
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name 'gnome-terminal'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command 'gnome-terminal'
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding '<Super>Return'

# shell and vim
cp {"configs/.zshrc","configs/.vimrc"} ~
cp configs/starship.toml ~/.config/
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +PlugInstall +qall
chsh -s /usr/bin/zsh

echo "Did you install VSCodium?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) tar -Jxf configs/vscode_extensions.tar.xz -C ~; mkdir -p ~/.config/VSCodium && tar -Jxf configs/vscode_settings.tar.xz -C ~/.config/VSCodium; break;;
		No ) break;;
	esac
done

# terminal font
curl -LO https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip
mkdir -p ~/.local/share/fonts && unzip FiraCode.zip -d ~/.local/share/fonts

