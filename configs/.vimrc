set clipboard=unnamedplus 
set ignorecase
set smartcase
"set inccommand=nosplit
syntax on
set hidden
set mouse=a

nnoremap <SPACE> <Nop>
let mapleader=" "

call plug#begin()

Plug 'scrooloose/nerdtree'
Plug 'unblevable/quick-scope'
Plug 'vim-airline/vim-airline'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'easymotion/vim-easymotion'

call plug#end()

"show highlights at cursor position
set cursorline 
set cursorcolumn 

"show relative line numbers
set number relativenumber

"set color theme
syntax enable
set background=dark
"colorscheme night-owl
hi VertSplit ctermbg=none
hi MatchParen cterm=bold ctermbg=none ctermfg=magenta
hi clear LineNr
hi LineNr ctermfg=23
highlight clear SignColumn

"Quick-scope
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline

"easymotion bindings
nmap <Leader>s <Plug>(easymotion-overwin-f)
" map <Leader>w <Plug>(easymotiod-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" nmap s <Plug>(easymotion-s2)
let g:EasyMotion_smartcase = 1
" hi link EasyMotionTarget ErrorMsg
" hi link EasyMotionShade  Comment
" hi link EasyMotionTarget2First MatchParen
" hi link EasyMotionTarget2Second MatchParen

"set autoindent
set autoindent

"finding files in subdirectories
set path+=**
set wildmenu
set wildmode=longest:full,full

"Nerdtree
let NERDTreeQuitOnOpen = 1
nmap <silent> <leader>q :NERDTreeToggle<CR>


"remapping of keys due to QWERTZ layout
nmap ö :
vmap ö :
nmap ä }
vmap ä }
nmap Ä {
vmap Ä {
nmap - /
nmap ü '

"remapping for switching windows in terminal mode
tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

"mapping reloading all buffers to F5
nnoremap <F5> :bufdo e<CR>    

"remapping of keys for easier navigation of split windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-RIGHT> <C-W>>
nnoremap <C-LEFT> <C-W><
nnoremap <C-UP> <C-W>+
nnoremap <C-DOWN> <C-W>-
set splitbelow
set splitright

"tabbing
nnoremap <C-LEFT> :tabprevious<CR>
nnoremap <C-RIGHT> :tabnext<CR>

"remapping of Ctrl-6 for switching of recent buffers
nnoremap <C-Q> <C-^>

"mapping of buffer list toggle
nnoremap <F3> :buffers<CR>:buffer<Space>
" autocmd BufWinEnter,WinEnter * if &buftype == 'terminal' | silent! normal i | endif

