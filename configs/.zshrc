# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install

# aliases
alias ll="exa -l"
alias lh="exa -lah"
alias lt="exa -lT"
alias lr="exa -ls modified"
alias upd="sudo dnf upgrade"
alias autorem="sudo dnf autoremove"
alias dnfs="dnf search"
alias dnfin="sudo dnf install"
alias dnfrem="sudo dnf remove"
alias zrc="vim ~/.zshrc"
alias vrc="vim ~/.vimrc"

# fish-like autosuggestions and syntax highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# Autosuggestions stuff
bindkey "^[[1;5C" forward-word
#bindkey "^[[1;5D" backward-word
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#505050"

# zoxide
eval "$(zoxide init --cmd y zsh)"

# starship prompt
eval "$(starship init zsh)"
